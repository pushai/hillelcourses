import java.util.Random;
import java.util.Scanner;

public class RpsGame {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            startGame(scanner);

            System.out.println("Play again? (y/n)");
            String restartGame = scanner.nextLine();

            if (!restartGame.equals("y")) break;
        }

        scanner.close();

    }

    public static void startGame(Scanner scanner) {
        String[] values = {"rock", "paper", "scissors"};
        String computerMove = values[new Random().nextInt(values.length)];
        String playerMove;

        while (true) {
            System.out.println("Your move! Enter rock, paper or scissors. Or r, p, s for short.");
            playerMove = scanner.nextLine();

            if (playerMove.equals("rock") ||
                    playerMove.equals("paper") ||
                    playerMove.equals("scissors") ||
                    playerMove.equals("r") ||
                    playerMove.equals("p") ||
                    playerMove.equals("s")) break;

            System.out.printf("\"%s\" - not valid value.\n", playerMove);
        }

        switch (playerMove) {
            case "r":
                playerMove = "rock";
                break;

            case "p":
                playerMove = "paper";
                break;

            case "s":
                playerMove = "scissors";
                break;
        }

        System.out.printf("Computer: %s, Player: %s \n", computerMove, playerMove);

        if (computerMove.equals(playerMove)) {
            System.out.println("Draw!");
        }

        switch (playerMove) {
            case "rock":
                if (computerMove.equals("paper")) {
                    System.out.println("You lose!");
                } else if (computerMove.equals("scissors")) {
                    System.out.println("You win!");
                } break;

            case "paper":
                if (computerMove.equals("scissors")) {
                    System.out.println("You lose!");
                } else if (computerMove.equals("rock")) {
                    System.out.println("You win!");
                } break;

            case "scissors":
                if (computerMove.equals("rock")) {
                    System.out.println("You lose!");
                } else if (computerMove.equals("paper")) {
                    System.out.println("You win!");
                } break;
        }
    }
}
